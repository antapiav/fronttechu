import { Component, OnInit } from '@angular/core';
import { Observable, timer } from 'rxjs';
import { take, map } from 'rxjs/operators';
import { AppUtils } from 'src/app/util/app.utils';
import { Usuario } from 'src/app/model/usuario.model';
import { Router } from '@angular/router';
import { AppConstants } from 'src/app/util/app.constants';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public today: Observable<number>;
  public usuario: Usuario;
  public rol: string;

  constructor(
    private router: Router
  ) {
    this.today = timer(0, 1000).pipe(
      take(Date.now()),
      map(() => Date.now())
    );
    this.usuario = new Usuario();
    this.rol = AppConstants.String.STRING_VACIO;
  }

  ngOnInit(): void {
        (AppUtils.validSession()) ? this.usuario = AppUtils.getSessionStorage() : this.usuario = new Usuario();
        switch(this.usuario.rol){
            case "1":
                this.rol = "admin";
                break;
            case "0":
                this.rol = "normal"
                break
        }
    }

  logout() {
      AppUtils.logOutSession();
      this.router.navigate(['/modulo-login/login']);
  }

}
