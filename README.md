![BBVA Continental](https://www.bbva.pe/content/dam/public-web/peru/images/logos/logo-bbva.png)

# Front Tech U

_App FRONT final practitioner_

Tech U

### Detalles

* Ramas: master, develop y feature/usuario
* Angular V9.1.9
* URL HEROKU: https://bbva-peru-tech-u.herokuapp.com/
* Usuario admin: antapiav@gmail.com
* Clave admin: admin

### Instalación en ambiente local

1. Clonar el repositorio
2. Instalar módulos de Node

```
npm install
```
3. Inicial app
```
ng serve --o
```